# Image Artefact Bot Post

A demonstrator repository that creates an image artefact in CI, and posts it as a comment to merge requests.


## License
[Unlicense](https://unlicense.org/). Use this however you like.
